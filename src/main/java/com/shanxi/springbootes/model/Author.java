package com.shanxi.springbootes.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Tony Zhang
 **/
@Data
@AllArgsConstructor
public class Author {
    private String name;

}
