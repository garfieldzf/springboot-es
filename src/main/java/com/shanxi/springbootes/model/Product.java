package com.shanxi.springbootes.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Tony Zhang
 **/
@Data
@ToString
@Accessors(chain = true)
@Document(indexName = "productindex")
public class Product {
    @Id
    private String id;

    @Field(type = FieldType.Text, name = "name")
    private String name;

    @Field(type = FieldType.Double, name = "price")
    private Double price;

    @Field(type = FieldType.Integer, name = "quantity")
    private Integer quantity;

    @Field(type = FieldType.Keyword, name = "category")
    private String category;

    @Field(type = FieldType.Keyword, name = "desc")
    private String description;

    @Field(type = FieldType.Date, name = "createdDateTime")
    private LocalDate createdDateTime;

    @Field(type = FieldType.Nested, includeInParent = true)
    private List<Author> authors;
}
