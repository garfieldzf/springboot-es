package com.shanxi.springbootes.service;

import com.shanxi.springbootes.model.Product;
import com.shanxi.springbootes.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Tony Zhang
 **/
@Service
public class ProductSearchService {

    private ProductRepository productRepository;

    public ProductSearchService(ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public void createProductIndex(Product product){
        this.productRepository.save(product);
    }
}
