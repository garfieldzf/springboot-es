package com.shanxi.springbootes.repository;

import com.shanxi.springbootes.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @author Tony Zhang
 **/
public interface ProductRepository extends ElasticsearchRepository<Product, String> {
    Product findByName(String name);

    @Query("{\"match\":{\"name\":{\"query\":\"?0\"}}}")
    Page<Product> findByName(String name, PageRequest pageRequest);

    Page<Product> findByNameContaining(String name, PageRequest pageRequest);


}
