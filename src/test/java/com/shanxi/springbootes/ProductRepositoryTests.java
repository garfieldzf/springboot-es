package com.shanxi.springbootes;

import com.shanxi.springbootes.model.Author;
import com.shanxi.springbootes.model.Product;
import com.shanxi.springbootes.repository.ProductRepository;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;

/**
 * @author Tony Zhang
 **/
@SpringBootTest
public class ProductRepositoryTests {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Test
    public void it_should_create_product_documents_successful(){

        List<Product> products = new ArrayList<>();

        for(int i =1;i<=30;i++){
            Product product =new Product();

            product.setName("python"+i)
                    .setPrice(30d)
                    .setQuantity(10)
                    .setCategory("book"+i)
                    .setDescription("a nice book"+i)
                    .setCreatedDateTime(LocalDate.now())
            ;

            product.setAuthors(Arrays.asList(new Author("李雷"),new Author("韩梅梅")));

            products.add(product);
        }

        this.productRepository.saveAll(products);
        Assertions.assertEquals(products.size(),30);
    }

    @Test
    public void it_should_call_findByNameContaining_success(){
        Page<Product> page = this.productRepository.findByNameContaining("python", PageRequest.of(1,10));
        for (Product product : page.toList()) {
            System.out.println(product.toString());
        }

        Assertions.assertEquals(page.toList().size(),10);
    }

    @Test
    public void it_should_call_findBy_successful(){
        Product product = this.productRepository.findByName("python2");

        Assertions.assertEquals(product.getQuantity(),10);
        Assertions.assertEquals(product.getPrice(),30d);
    }

    @Test
    public void it_should_update_Product_successful(){
        Product product =  this.productRepository.findByName("python1");
        product.setQuantity(15);

        this.productRepository.save(product);

        Assertions.assertEquals(15, product.getQuantity());

    }

    @Test
    public void it_should_delete_Product_successful(){
        Product product = this.productRepository.findById("ygnG3n4B9LwueG0uoDc2").get();
        this.productRepository.delete(product);

    }

    @Test
    public void it_should_delete_index_successful(){
        this.elasticsearchRestTemplate.indexOps(Product.class).delete();

        this.elasticsearchRestTemplate.indexOps(Product.class).create();
    }

    @Test
    public void it_should_delete_documents_successful(){

        this.productRepository.deleteAll();
    }

    @Test
    public void multi_match_query(){
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(multiMatchQuery("nice")
                        .field("name")
                        .field("category")
                        .field("desc").type(MultiMatchQueryBuilder.Type.BEST_FIELDS)).build();

        SearchHits<Product> products = elasticsearchRestTemplate
                .search(searchQuery,Product.class, IndexCoordinates.of("productindex"));


    }

    @Test
    public void it_should_using_query_successful(){
        Page<Product> products = this.productRepository.findByName("python1",PageRequest.of(0,10));
        products.toList().forEach(x->{
            System.out.println(x.getName()+","+x.getCategory());
        });

    }

}
